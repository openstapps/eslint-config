# @openstapps/eslint-config

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/eslint-config.svg?style=flat-square)](https://gitlab.com/openstapps/eslint-config/commits/master)
[![npm](https://img.shields.io/npm/v/@openstapps/eslint-config.svg?style=flat-square)](https://npmjs.com/package/@openstapps/eslint-config)
[![license)](https://img.shields.io/npm/l/@openstapps/eslint-config.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Installation

Put this into your `.eslintrc`

```json
{
  "extends": "@openstapps"
}
```

Unfortunately, ESLint requires you to define plugins and configs
your config depends on as `peerDependencies`, which means they
have to be installed manually.

Use the command

```shell
npx install-peerdeps -od --extra-args "-E" @openstapps/eslint-config
```

Or, alternatively, add the following to your `package.json`:

```json
{
  "devDependencies": {
    "@typescript-eslint/eslint-plugin": ">=5.29.0",
    "@typescript-eslint/parser": ">=5.29.0",
    "eslint": ">=8.18.0",
    "eslint-config-prettier": ">=8.5.0",
    "eslint-plugin-jsdoc": ">=39.3.3",
    "eslint-plugin-prettier": ">=4.0.0",
    "eslint-plugin-unicorn": ">=41.0.1",
    "prettier": ">=2.7.1"
  }
}
```
