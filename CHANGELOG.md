# [1.1.0](https://gitlab.com/openstapps/eslint-config/compare/v1.0.0...v1.1.0) (2022-06-27)



# [1.0.0](https://gitlab.com/openstapps/eslint-config/compare/v0.0.3...v1.0.0) (2022-05-05)


### Features

* support es-mapping tags ([418fc6b](https://gitlab.com/openstapps/eslint-config/commit/418fc6ba0532bfe0c368f90f2bbcf7f47c091e11))



## [0.0.3](https://gitlab.com/openstapps/eslint-config/compare/v0.0.2...v0.0.3) (2022-04-25)



## [0.0.2](https://gitlab.com/openstapps/eslint-config/compare/bc15df828ddad4be4804d7ff87828c9c8ddd6f46...v0.0.2) (2022-04-25)


### Features

* gitlab-ci ([bc15df8](https://gitlab.com/openstapps/eslint-config/commit/bc15df828ddad4be4804d7ff87828c9c8ddd6f46))



