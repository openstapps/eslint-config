/**
 * Simple TS source file to get linted
 * Can be used to verify new/changed rule behavior
 */
function addNumbers(a: number, b: number) {
  return a + b;
}

const sum: number = addNumbers(10, 15);

// eslint-disable-next-line no-console
console.log('Sum of the two numbers is: ' + sum);
